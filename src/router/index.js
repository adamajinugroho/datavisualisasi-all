import Vue from 'vue'
import Router from 'vue-router'

import About from '../components/about'
import Fact from '../components/fact'
import Line from '../components/linechart'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/about'
    },
    {
      path: '/fact',
      name: 'Fact',
      component: Fact
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/map',
      name: 'Map',
      component: Line
    }
  ],
  mode: 'history'
})
